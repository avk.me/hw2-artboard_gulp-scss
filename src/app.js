const menuBtn = document.querySelector(".header__menu-icon");
const mobileMenu = document.querySelector(".header__mobile-menu");
const closeMobileMenu = document.querySelector(".header__menu-icon__x");
const openMobileMenu = document.querySelectorAll(".header__menu-icon__stick");

menuBtn.addEventListener("click", function () {
    mobileMenu.classList.toggle('menu--isActive');
    if (mobileMenu.classList.contains('menu--isActive')) {
        closeMobileMenu.style.display = "block";
        openMobileMenu.forEach (m => {
            m.style.display = "none";
        })
    } else {
        closeMobileMenu.style.display = "none";
        openMobileMenu.forEach (m => {
            m.style.display = "block";
        })
    }
})

