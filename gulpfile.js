const gulp = require('gulp');
const clean = require('gulp-clean');
const sass = require('gulp-sass')(require('sass'));
const autoprefixer = require('gulp-autoprefixer');
const cleanCSS = require('gulp-clean-css');
const babel = require('gulp-babel');
const uglify = require('gulp-uglify');
const concat = require('gulp-concat');
const rename = require("gulp-rename");
const imagemin = require('gulp-imagemin');
const browserSync = require('browser-sync').create();

const cleanDist = function () {
    return gulp.src('./dist', {
            read: false
        })
        .pipe(clean());
}

const compileSass = function () {
    return gulp.src('./src/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['> 0.01%'],
            cascade: false
        }))
        .pipe(cleanCSS({
            compatibility: 'ie8'
        }))
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest('./src'))
        .pipe(browserSync.stream());
}

const jsConverter = function () {
    return gulp.src('./src/app.js')
        .pipe(babel({
            presets: ['@babel/env']
        }))
        .pipe(uglify())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest('./src'))
        .pipe(browserSync.stream());
}

const copyCss = function () {
    return gulp.src("./src/*.min.css")
        .pipe(gulp.dest("./dist"))
}

const copyScripts = function () {
    return gulp.src("src/**/*.min.js")
        .pipe(gulp.dest("./dist"))
}

const minifyImg = function() {
	return gulp.src('./src/img/*')
		.pipe(imagemin())
		.pipe(gulp.dest('./dist/img'))
}

gulp.task('dev', function(){
    browserSync.init({
        server: "./"
    });
   
    gulp.watch('./src/**/*.scss', cleanDist);
    gulp.watch('./src/**/app.js', cleanDist);
    gulp.watch('./src/**/*.scss', compileSass);
    gulp.watch('./src/**/app.js', jsConverter);
    gulp.watch('./src/**/*.min.css', copyCss);
    gulp.watch('./src/**/*.min.css', copyScripts);
    gulp.watch('./src/**/*.min.js', copyScripts);
    gulp.watch('./src/**/*.min.js', copyCss);
    minifyImg();
    gulp.watch('./src/img', minifyImg);
    gulp.watch('*.html').on('change', browserSync.reload);
})

gulp.task('build', function () {
    gulp.watch('./src/**/*.scss', cleanDist);
    gulp.watch('./src/**/app.js', cleanDist);
    gulp.watch('./src/**/*.scss', compileSass);
    gulp.watch('./src/**/app.js', jsConverter);
    gulp.watch('./src/**/*.min.css', copyCss);
    gulp.watch('./src/**/*.min.css', copyScripts);
    gulp.watch('./src/**/*.min.js', copyScripts);
    gulp.watch('./src/**/*.min.js', copyCss);
    minifyImg();
    gulp.watch('./src/img', minifyImg);
});